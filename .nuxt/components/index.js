export { default as CommonFooter } from '../../components/commonFooter.vue'
export { default as CommonHeader } from '../../components/commonHeader.vue'
export { default as DiscographyContents } from '../../components/discographyContents.vue'
export { default as KeyVisual } from '../../components/keyVisual.vue'

export const LazyCommonFooter = import('../../components/commonFooter.vue' /* webpackChunkName: "components/commonFooter'}" */).then(c => c.default || c)
export const LazyCommonHeader = import('../../components/commonHeader.vue' /* webpackChunkName: "components/commonHeader'}" */).then(c => c.default || c)
export const LazyDiscographyContents = import('../../components/discographyContents.vue' /* webpackChunkName: "components/discographyContents'}" */).then(c => c.default || c)
export const LazyKeyVisual = import('../../components/keyVisual.vue' /* webpackChunkName: "components/keyVisual'}" */).then(c => c.default || c)
