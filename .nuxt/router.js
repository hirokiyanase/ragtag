import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _349931de = () => interopDefault(import('../pages/biography.vue' /* webpackChunkName: "pages/biography" */))
const _7e411ae4 = () => interopDefault(import('../pages/contact/index.vue' /* webpackChunkName: "pages/contact/index" */))
const _1a39f4da = () => interopDefault(import('../pages/discography/index.vue' /* webpackChunkName: "pages/discography/index" */))
const _21cfcee0 = () => interopDefault(import('../pages/music.vue' /* webpackChunkName: "pages/music" */))
const _e6beeb08 = () => interopDefault(import('../pages/schedule.vue' /* webpackChunkName: "pages/schedule" */))
const _abd5df82 = () => interopDefault(import('../pages/contact/success.vue' /* webpackChunkName: "pages/contact/success" */))
const _b505d7de = () => interopDefault(import('../pages/discography/ragtag.vue' /* webpackChunkName: "pages/discography/ragtag" */))
const _dbef9722 = () => interopDefault(import('../pages/discography/thesharingsong.vue' /* webpackChunkName: "pages/discography/thesharingsong" */))
const _b0959686 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/biography",
    component: _349931de,
    name: "biography"
  }, {
    path: "/contact",
    component: _7e411ae4,
    name: "contact"
  }, {
    path: "/discography",
    component: _1a39f4da,
    name: "discography"
  }, {
    path: "/music",
    component: _21cfcee0,
    name: "music"
  }, {
    path: "/schedule",
    component: _e6beeb08,
    name: "schedule"
  }, {
    path: "/contact/success",
    component: _abd5df82,
    name: "contact-success"
  }, {
    path: "/discography/ragtag",
    component: _b505d7de,
    name: "discography-ragtag"
  }, {
    path: "/discography/thesharingsong",
    component: _dbef9722,
    name: "discography-thesharingsong"
  }, {
    path: "/",
    component: _b0959686,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
